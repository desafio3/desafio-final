# Proposta Histórico Fatura

Primeiramente obrigado por estar aqui! E querer trabalhar no Itaú Unibanco!.

Esse é nosso desfio 3 de 3, nosso desafio final, onde iremos avaliar sua habilidade com frameworks Javascript, requisições http, implementação de um design system e programação funcional.
Para isso você ira consumir uma API onde estão os lançamentos de um cartão de credito dentro de um mesmo ano. Instruções [**neste link**](https://gitlab.com/desafio3/readme-api)

O método de avaliação é simples, tendo seu código em mãos iremos rodar o projeto e validar se você construiu o projeto atendendo ao cenário proposto e os requisitos.

Boa sorte!

### Cenário Proposto

_Usando a mesma API do desafio anterior_
_Você deve criar o front-end, com aspecto de app(iremos chamar de app), usando seus frameworks de preferência._  
_Você terá que criar um app baseado em seu Design System de preferência, recomendamos o Material Design do Google, esse app deve conter 3 abas que irão agrupar os gastos das seguntes formas: **Lista Geral**, **Por Mês** e **Por Categoria**._

### Requisitos

* O framework utilizado deve ser um framework de Javascript - Preferencialmente Angular2+.
* Seu app deve ser responsivo, tendo a aparência de um aplicativo, usando seu Design System de prefêrencia, recomendamos o Matrial Desgin do Google https://material.angular.io/ para Angular, https://material.io/ para outros frameworks.
* A tela do seu app deve conter 3 abas **Lista Geral, Por Mês e Por Categoria.**. Exemplo [**neste link**](https://gitlab.com/desafio3/desafio-final/-/blob/master/Experiencia/Experiencia.mov).
* Só devem ser exibidos os meses em que houveram lançamentos.
* Os meses e categorias devem ser exibidos por escrito sempre.
* Essa tarefa deve ser entregue em um repositório público no Gitlab, o link desse repositório deve ser enviado para Felipe Marques Melo felipe.melo@itau-unibanco.com.br com seu nome completo


### Dica

* Atente-se a organização do projeto, isso será levado em conta.
* O uso de programação funcional irá te ajudar muito nesse exercício.
* O Design System escolhido não impacta na avaliação.

**ATENÇÃO:** _Após o envio do repositório para o e-mail descrito nos requisitos não serão aceitos novos commits, essa atividade deve ser enviada até _ 

### Bom Desempenho!

### #QueremosVoceNoItau